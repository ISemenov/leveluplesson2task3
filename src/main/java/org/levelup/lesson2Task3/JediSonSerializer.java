package org.levelup.lesson2Task3;

import org.levelup.lesson2Task3.SerializationUtils.Serializer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JediSonSerializer implements Serializer {

    @Override
    public String serialize(Object object) {
        List<Field> fields = new ArrayList<>();
        getAllFields(fields, object.getClass());
        StringBuilder result = new StringBuilder(object.getClass().getSimpleName());
        result.append("{\n");

        for (Field field : fields) {
            try {
                field.setAccessible(true);
                result.append("\t").append(field.getName()).append(" : ").append(field.get(object)).append("\n");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        result.append("}");
        return result.toString();
    }

    private List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }
}
