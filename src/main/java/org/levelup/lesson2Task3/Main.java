package org.levelup.lesson2Task3;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException, IOException {

        Jedi jedi = new Jedi("Obi-Van Kenobi", 67, Jedi.MasteryLevel.MASTER, "blue");
        ImperialSpy imperialSpy = new ImperialSpy();
        imperialSpy.encryptMessage(jedi);

        StringExchangerSystem theEmpire = new StringExchangerSystem();
        theEmpire.receiveMessage(imperialSpy.sendMessage());

        System.out.println(theEmpire.sendMessage());

        imperialSpy.encryptMessage(new Person("some guy"));
        theEmpire.receiveMessage(imperialSpy.sendMessage());

        System.out.println(theEmpire.sendMessage());
        
    }
}
