package org.levelup.lesson2Task3;

import org.levelup.lesson2Task3.SerializationUtils.SerializationProcessor;

import java.lang.reflect.InvocationTargetException;

public class StringExchangerSystem {

    private static final SerializationProcessor PROCESSOR = new SerializationProcessor();

    private String encryptedMessage;

    public StringExchangerSystem() {
    }

    public String sendMessage() {
        return encryptedMessage;
    }

    public void encryptMessage(Object object) throws InstantiationException, IllegalAccessException, InvocationTargetException {
        this.encryptedMessage = PROCESSOR.process(object);
    }

    public void receiveMessage(String encryptedMessage){
        this.encryptedMessage = encryptedMessage;
    }

}
