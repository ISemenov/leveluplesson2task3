package org.levelup.lesson2Task3;

import org.levelup.lesson2Task3.SerializationUtils.SerializedBy;

import java.util.Objects;

@SerializedBy(value = JediSonSerializer.class)
public class Jedi extends Person {

    public Jedi(String name) {
        super(name);
    }

    public Jedi(String name, int age) {
        super(name, age);
    }

    public Jedi(String name, MasteryLevel masteryLevel) {
        super(name);
        this.masteryLevel = masteryLevel;
    }

    public Jedi(String name, int age, MasteryLevel masteryLevel) {
        super(name, age);
        this.masteryLevel = masteryLevel;
    }

    public Jedi(String name, MasteryLevel masteryLevel, String swordColor) {
        super(name);
        this.masteryLevel = masteryLevel;
        this.swordColor = swordColor;
    }

    public Jedi(String name, int age, MasteryLevel masteryLevel, String swordColor) {
        super(name, age);
        this.masteryLevel = masteryLevel;
        this.swordColor = swordColor;
    }

    enum MasteryLevel {
        PADAVAN,
        KNIGHT,
        MASTER
    }

    private MasteryLevel masteryLevel;

    private String swordColor;

    public MasteryLevel getMasteryLevel() {
        return masteryLevel;
    }

    public void setMasteryLevel(MasteryLevel masteryLevel) {
        this.masteryLevel = masteryLevel;
    }

    public String getSwordColor() {
        return swordColor;
    }

    public void setSwordColor(String swordColor) {
        this.swordColor = swordColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Jedi)) return false;
        Jedi jedi = (Jedi) o;
        return getMasteryLevel() == jedi.getMasteryLevel() &&
                Objects.equals(getSwordColor(), jedi.getSwordColor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMasteryLevel(), getSwordColor());
    }

    @Override
    public String toString() {
        return "Jedi{" +
                "masteryLevel=" + masteryLevel +
                ", swordColor='" + swordColor + '\'' +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                '}';
    }
}
