package org.levelup.lesson2Task3.SerializationUtils.exceptions;

public class SerializationException extends RuntimeException {

    public SerializationException(String s) {
        super(s);
    }

    public SerializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
