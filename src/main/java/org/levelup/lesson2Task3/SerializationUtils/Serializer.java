package org.levelup.lesson2Task3.SerializationUtils;

public interface Serializer {

    String serialize(Object object);
}
