package org.levelup.lesson2Task3.SerializationUtils;

import org.levelup.lesson2Task3.SerializationUtils.exceptions.SerializationException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SerializationProcessor {

    public String process(Object o) throws IllegalAccessException, InstantiationException, InvocationTargetException, SerializationException {
        Class<?> aClass = o.getClass();
        SerializedBy serializedBy = aClass.getAnnotation(SerializedBy.class);
        if(serializedBy == null){
            throw new SerializationException("Class " + aClass.getName() + " has to be annotated with " +
                    "'@SerializedBy' annotation!");
        }
        Class<?> serializerClass = serializedBy.value();
        String result;
        try {
            Method serialize = serializerClass.getMethod("serialize", Object.class);
            Object serializer = serializerClass.newInstance();
            result = (String) serialize.invoke(serializer, o);
        } catch (NoSuchMethodException e) {
            throw new SerializationException(serializerClass.getName() + " has to implement Serializer.class", e);
        }
        return result;
    }
    
}
